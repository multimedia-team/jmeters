// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include <string.h>


#ifndef __GLOBAL_H
#define __GLOBAL_H


#define  EV_X11         16
#define  EV_EXIT        31
#define  NMAX           64


class JMconf
{
public:

    JMconf (void) :
        _mtype (0),
        _frame (0),
        _nchan (0),
        _ninp (0),
        _ncol (4)
    {
	memset (_jport, 0, NMAX * sizeof (void *));
	memset (_label, 0, NMAX * sizeof (void *));
    }

    int          _mtype;
    int          _frame;
    int          _nchan;
    int          _ninp;
    int          _ncol;
    float        _gain;
    const char  *_jport [NMAX];
    const char  *_label [NMAX];
};


#endif
