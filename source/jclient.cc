// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include <string.h>
#include <sndfile.h>
#include <math.h>
#include "jclient.h"
#include "meterwin.h"


Jclient::Jclient (const char *name, JMconf *config) :
    A_thread ("Jclient"),
    _jack_client (0),
    _jname (name),
    _fsamp (0),
    _fsize (0),
    _config (config),
    _type (0),
    _vudsp (0),
    _i1dsp (0),
    _i2dsp (0),
    _scdsp (0)
{
    init_jack ();   
    initialise ();
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
    delete[] _vudsp;
    delete[] _i1dsp;
    delete[] _i2dsp;
    delete[] _scdsp;
}


void Jclient::init_jack (void)
{
    int            i;
    char           t1 [16];
    char           t2 [64];
    jack_status_t  s;

    if ((_jack_client = jack_client_open (_jname, (jack_options_t) 0, &s)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK.\n");
        exit (1);
    }

    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);

    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }

    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);
    _fsize = jack_get_buffer_size (_jack_client);

    for (i = 0; i < _config->_ninp; i++)
    {
	sprintf (t1, "in-%d", i + 1);
	sprintf (t2, "%s:%s", _jname, t1);
        _jack_ipports [i] = jack_port_register (_jack_client, t1, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
	if (_config->_jport [i])
	{
  	    jack_connect (_jack_client, _config->_jport [i], t2);
	}
    }
}


void Jclient::close_jack ()
{
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    return ((Jclient *) arg)->jack_shutdown ();
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}
 

int Jclient::jack_process (jack_nframes_t nframes)
{
    int    i, j;
    float  *p1, *p2;

    switch (_type)
    {
    case VU:
	for (i = 0; i < _config->_ninp; i++)
	{
	    p1 = (float *) jack_port_get_buffer (_jack_ipports [i], nframes);
	    _vudsp [i].process (p1, nframes);
	}
	break;

    case IEC1:
	for (i = 0; i < _config->_ninp; i++)
	{
	    p1 = (float *) jack_port_get_buffer (_jack_ipports [i], nframes);
	    _i1dsp [i].process (p1, nframes);
	}
	break;

    case IEC2:
	for (i = 0; i < _config->_ninp; i++)
	{
	    p1 = (float *) jack_port_get_buffer (_jack_ipports [i], nframes);
	    _i2dsp [i].process (p1, nframes);
	}
	break;

    case STC:
	for (i = j = 0; i < _config->_ninp / 2; i++)
	{
	    p1 = (float *) jack_port_get_buffer (_jack_ipports [j++], nframes);
	    p2 = (float *) jack_port_get_buffer (_jack_ipports [j++], nframes);
	    _scdsp [i].process (p1, p2, nframes);
	}
	break;
    }

    return 0;
}


void Jclient::initialise ()
{
    switch (_config->_mtype)
    {
    case Meterwin::VUM:
	_type = VU;
	_vudsp = new Vumeterdsp [_config->_ninp];
	Vumeterdsp::init (_fsamp);
	break;
    case Meterwin::DIN:
	_type = IEC1;
	_i1dsp = new Iec1ppmdsp [_config->_ninp];
	Iec1ppmdsp::init (_fsamp);
	break;
    case Meterwin::BBC:
    case Meterwin::EBU:
	_type = IEC2;
	_i2dsp = new Iec2ppmdsp [_config->_ninp];
	Iec2ppmdsp::init (_fsamp);
	break;
    case Meterwin::CVUM:
    case Meterwin::CPPM:
    case Meterwin::CDIN:
	_type = STC;
	_scdsp = new Stcorrdsp [_config->_ninp / 2];
	Stcorrdsp::init (_fsamp, 2e3f, 0.3f);
	break;
    }
}


float Jclient::read (int i)
{
    switch (_type)
    {
    case VU:
	return _vudsp [i].read ();
    case IEC1:
	return _i1dsp [i].read ();
    case IEC2:
	return _i2dsp [i].read ();
    case STC:
	return _scdsp [i].read ();
    }
    return 0;
}

