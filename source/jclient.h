// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <clthreads.h>
#include <jack/jack.h>
#include "global.h"
#include "vumeterdsp.h"
#include "iec1ppmdsp.h"
#include "iec2ppmdsp.h"
#include "stcorrdsp.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *name, JMconf *config);
    ~Jclient (void);

    const char   *jname (void) const { return _jname; }
    unsigned int  fsamp (void) const { return _fsamp; }

    float read (int i);

private:

    enum { VU = 1, IEC1, IEC2, STC };

    void init_jack (void);
    void close_jack (void);
    void jack_shutdown (void);
    int  jack_process (jack_nframes_t nframes);

    virtual void thr_main (void) {}

    void initialise (void);

    jack_client_t  *_jack_client;
    jack_port_t    *_jack_ipports [NMAX];
    const char     *_jname;
    unsigned int    _fsamp;
    unsigned int    _fsize;
    JMconf         *_config;
    int             _type;
    Vumeterdsp     *_vudsp;
    Iec1ppmdsp     *_i1dsp;
    Iec2ppmdsp     *_i2dsp;
    Stcorrdsp      *_scdsp;
    

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
