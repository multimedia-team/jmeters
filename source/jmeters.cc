// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <sys/mman.h>
#include <signal.h>
#include "styles.h"
#include "mainwin.h"


#define NOPTS 8 
#define CP (char *)


XrmOptionDescRec options [NOPTS] =
{
    {CP"-help",      CP".help",     XrmoptionNoArg,   CP"true"    },
    {CP"-type",      CP".type",     XrmoptionSepArg,  CP"vu"      },
    {CP"-frame",     CP".frame",    XrmoptionSepArg,  CP"rect"    },
    {CP"-reflev",    CP".reflev",   XrmoptionSepArg,  CP"0"       },
    {CP"-name",      CP".name",     XrmoptionSepArg,  CP"jmeters" },
    {CP"-update",    CP".update",   XrmoptionSepArg,  CP"20"      },
    {CP"-columns",   CP".columns",  XrmoptionSepArg,  CP"4"       },
    {CP"-geometry",  CP".geometry", XrmoptionSepArg,  CP""        }
};


static Jclient  *jclient = 0;
static Mainwin  *mainwin = 0;
static JMconf    config;


static void help (void)
{
    fprintf (stderr, "\nJmeters-%s\n", VERSION);
    fprintf (stderr, "(C) 2008-2012 Fons Adriaensen  <fons@linuxaudio.org>\n");
    fprintf (stderr, "Syntax: jmeters <options> <source | label>+\n");
    fprintf (stderr, "Options:\n");
    fprintf (stderr, "  -h         Display this text\n");
    fprintf (stderr, "  -t <type>  Meter type (vu, bbc, ebu, din, sbbc, sebu, sdin, cvu, cppm, cdin)\n");
    fprintf (stderr, "  -f <frame> Meter frame (rect, small, mbrl, mbrs)\n");
    fprintf (stderr, "  -r <v>     Reference level change (0 dB)\n");
    fprintf (stderr, "  -n <name>  Name to use as Jack client (jmeters)\n");
    fprintf (stderr, "  -u <n>     Update rate (20)\n");
    fprintf (stderr, "  -c <n>     Columns (4)\n");
    fprintf (stderr, "  -g <n>     Window geometry\n");
    exit (1);
}


static void sigint_handler (int)
{
    signal (SIGINT, SIG_IGN);
    mainwin->stop ();
}


int main (int ac, char *av [])
{
    X_resman      xresman;
    X_display     *display;
    X_handler     *handler;
    X_rootwin     *rootwin;
    const char    *p;
    int           i, ev, np, nl;
    
    xresman.init (&ac, av, CP"jmeters", options, NOPTS);
    if (xresman.getb (".help", 0)) help ();
            
    display = new X_display (xresman.get (".display", 0));
    if (display->dpy () == 0)
    {
	fprintf (stderr, "Can't open display.\n");
        delete display;
	exit (1);
    }

    p = xresman.get (".frame", "rect");
    if      (! strcmp (p, "rect"))  config._frame = Meterwin::RECT;
    else if (! strcmp (p, "small")) config._frame = Meterwin::SMALL;
    else if (! strcmp (p, "mbrl"))  config._frame = Meterwin::MBRL;
    else if (! strcmp (p, "mbrs"))  config._frame = Meterwin::MBRS;
    else help ();

    p = xresman.get (".type", "vu");
    if (! strcmp (p, "vu"))
    {
        config._mtype = Meterwin::VUM;
	config._nchan = 1;
    }
    else if (! strcmp (p, "bbc"))
    {
        config._mtype = Meterwin::BBC;
	config._nchan = 1;
    }
    else if (! strcmp (p, "ebu"))
    {
        config._mtype = Meterwin::EBU;
	config._nchan = 1;
    }
    else if (! strcmp (p, "din"))
    {
        config._mtype = Meterwin::DIN;
	config._nchan = 1;
    }
    else if (! strcmp (p, "sbbc"))
    {
        config._mtype = Meterwin::BBC;
	config._nchan = 2;
    }
    else if (! strcmp (p, "sebu"))
    {
        config._mtype = Meterwin::EBU;
	config._nchan = 2;
    }
    else if (! strcmp (p, "sdin"))
    {
        config._mtype = Meterwin::DIN;
	config._nchan = 2;
    }
    else if (! strcmp (p, "cvu"))
    {
        config._mtype = Meterwin::CVUM;
	if (config._frame == Meterwin::RECT) config._frame = Meterwin::SMALL;
	config._nchan = 2;
    }
    else if (! strcmp (p, "cppm"))
    {
        config._mtype = Meterwin::CPPM;
	if (config._frame == Meterwin::RECT) config._frame = Meterwin::SMALL;
	config._nchan = 2;
    }
    else if (! strcmp (p, "cdin"))
    {
        config._mtype = Meterwin::CDIN;
	config._frame = Meterwin::SMALL;
	config._nchan = 2;
    }
    else help ();

    if (--ac < 1) help ();
    if (ac > NMAX) ac = NMAX;
    config._ncol = atoi (xresman.get (".columns", "4"));
    config._gain = atof (xresman.get (".reflev", "0"));
    for (i = np = nl = 0; i < ac; i++)
    {
	p = *++av;
	if (strchr (p, ':'))  config._jport [np++] = p;			 
	else                  config._label [nl++] = p;
    }
    if (np == 0) np = config._nchan * nl;
    if (np == 0) help ();
    if (np % config._nchan) help ();
    config._ninp = np;

    if (config._ninp == 0) help ();

    styles_init (display, &xresman);
    rootwin = new X_rootwin (display);
    jclient = new Jclient (xresman.rname (), &config);
    mainwin = new Mainwin (rootwin, &xresman, jclient, &config);
    handler = new X_handler (display, mainwin, EV_X11);
    handler->next_event ();
    XFlush (display->dpy ());

    if (mlockall (MCL_CURRENT | MCL_FUTURE)) fprintf (stderr, "Warning: memory lock failed.\n");
    signal (SIGINT, sigint_handler); 
    ITC_ctrl::connect (jclient, EV_EXIT, mainwin, EV_EXIT);

    do
    {
	ev = mainwin->process ();
	if (ev == EV_X11)
	{
	    rootwin->handle_event ();
	    handler->next_event ();
	}
    }
    while (ev != EV_EXIT);	

    delete jclient;
    delete handler;
    delete rootwin;
    delete display;

    return 0;
}



