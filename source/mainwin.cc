// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "styles.h"
#include "global.h"
#include "mainwin.h"
#include "png2img.h"


Mainwin::Mainwin (X_rootwin *parent, X_resman *xres, Jclient *jclient, JMconf *config) :
    A_thread ("Main"),
    X_window (parent, 100, 100, 100, 100, XftColors [C_MAIN_BG]->pixel),
    _xres (xres),
    _stop (false),
    _pixm (0),
    _jclient (jclient),
    _config (config),
    _dx (0),
    _dy (0),
    _dt (50000),
    _dg (1)
{
    int       xp, yp, xs, ys, xx, yy;
    int       i, j, x, y;
    char      s [1024];
    X_hints   H;
  
    _atom = XInternAtom (dpy (), "WM_DELETE_WINDOW", True);
    XSetWMProtocols (dpy (), win (), &_atom, 1);
    _atom = XInternAtom (dpy (), "WM_PROTOCOLS", True);

    j = atoi (xres->get (".update", "20"));
    if (j < 10) j = 10;
    if (j > 50) j = 50;
    _dt = 1000000 / j;
    _dg = powf (10.0f, 0.05f * _config->_gain);
    _nc = config->_ninp;
    _nm = _nc / _config->_nchan;;
    j = y = 0;
    x = 0;
    for (i = 0; i < _nm; i++) 
    {
	if (j == config->_ncol)
	{
	    j = 0;
            x = 0;
	    y += _dy;
	}
	_meters [i] = new Meterwin (this, x, y, XftColors [C_MAIN_BG],
                                    _config->_mtype, _config->_frame, _config->_nchan,
                                    SHARED, _config->_label [i]);
	if (i == 0)
	{
	    _dx = _meters [i]->xs ();
	    _dy = _meters [i]->ys ();
	}
	x += _dx;
	j += 1;
    }	

    sprintf (s, "%s    %s-%s", jclient->jname (), PROGRAM, VERSION);
    x_set_title (s);
    j = (_nm < config->_ncol) ? _nm : config->_ncol;
    xp = 100;
    yp = 100;
    xs = j * _dx;
    ys = y + _dy;
    H.position (100, 100);
    H.minsize (100, 100);
    H.maxsize (xs, ys);
    H.rname (xres->rname ());
    H.rclas (xres->rclas ());
    x_apply (&H); 
    xres->geometry (".geometry", disp ()->xsize (), disp ()->ysize (), 1, xp, yp, xx, yy);
    x_moveresize (xp, yp, xs, ys);

    x_map (); 
    XFlush (dpy ());
    set_time (0);
    inc_time (100000);
}

 
Mainwin::~Mainwin (void)
{
    if (_pixm) XFreePixmap (dpy (), _pixm);
}

 
int Mainwin::process (void)
{
    int e;

    if (_stop) handle_stop ();
    e = get_event_timed ();
    switch (e)
    {
    case EV_TIME:
        handle_time ();
	break;
    }
    return e;
}


void Mainwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case Expose:
	expose ((XExposeEvent *) E);
	break;  
 
    case ClientMessage:
        clmesg ((XClientMessageEvent *) E);
        break;
    }
}


void Mainwin::expose (XExposeEvent *E)
{
    if (E->count) return;
    redraw ();
}


void Mainwin::clmesg (XClientMessageEvent *E)
{
    if (E->message_type == _atom) _stop = true;
}


void Mainwin::handle_time (void)
{
    int i, j, n;
    float v1, v2;

    n = _config->_nchan;
    for (i = j = 0; i < _nm; i++)
    {
	if (n >= 1) v1 = _jclient->read (j++);
	else v1 = 0;
	if (n >= 2) v2 = _jclient->read (j++);  
	else v2 = 0;
	_meters [i]->set_val (_dg * v1, _dg * v2);
    }

    XFlush (dpy ());
    inc_time (_dt);
}


void Mainwin::handle_stop (void)
{
    put_event (EV_EXIT, 1);
}


void Mainwin::handle_callb (int type, X_window *W, XEvent *E)
{
}


void Mainwin::update (void)
{
}


void Mainwin::redraw (void)
{
}


void Mainwin::addtext (X_window *W, X_textln_style *T, int xp, int yp, int xs, int ys, const char *text, int align)
{
    (new X_textln (W, T, xp, yp, xs, ys, text, align))->x_map ();
}


