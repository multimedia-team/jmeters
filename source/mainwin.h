// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#ifndef __MAINWIN_H
#define	__MAINWIN_H


#include <clxclient.h>
#include "global.h"
#include "jclient.h"
#include "meterwin.h"



class Mainwin : public A_thread, public X_window, public X_callback
{
public:

    Mainwin (X_rootwin *parent, X_resman *xres, Jclient *jclient, JMconf *config);
    ~Mainwin (void);
    Mainwin (const Mainwin&);
    Mainwin& operator=(const Mainwin&);

    void stop (void) { _stop = true; }
    void loadconf (void);
    int  process (void);

private:

    virtual void thr_main (void) {}

    void handle_time (void);
    void handle_stop (void);
    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void expose (XExposeEvent *E);
    void clmesg (XClientMessageEvent *E);
    void makepixm (const char *file, XftColor *bgnd);
    void addtext (X_window *W, X_textln_style *T, int xp, int yp, int xs, int ys, const char *text, int align);
    void update (void);
    void redraw (void);

    X_resman       *_xres;
    Atom            _atom;
    bool            _stop;
    Pixmap          _pixm;
    Jclient        *_jclient;
    JMconf         *_config;
    Meterwin       *_meters [NMAX];
    int             _dx;
    int             _dy;
    int             _dt;
    int             _nm;
    int             _nc;
    float           _dg;
};


#endif
