// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include <math.h>
#include "meterwin.h"
#include "png2img.h"
#include "styles.h"


cairo_t         *Meterwin::_cairotype = 0;
cairo_surface_t *Meterwin::_cairosurf = 0;
Jmpixmap        *Meterwin::_pixmaps = 0;
double           Meterwin::_col_vum [3] = { 0, 0, 0 };
double           Meterwin::_col_ppm [3] = { 1, 1, 1 };
double           Meterwin::_col_ppmL [3] = { 1, 0, 0 };
double           Meterwin::_col_ppmR [3] = { 0, 1, 0 };


Meterwin::Meterwin (X_window *parent, int xpos, int ypos, XftColor *bgnd,
		    int mtype, int frame, int nchan, const char *pngpath,
                    const char *label) :
    X_window (parent, xpos, ypos, 100, 100, bgnd->pixel),
    _mtype (mtype),
    _frame (frame),
    _label (label),
    _bgnd (bgnd),
    _xs (100),
    _ys (100),
    _col1 (0),
    _col2 (0),
    _val1 (0),
    _val2 (0)
{
    _pixm = findpixmap (mtype, frame, pngpath);
    if (! _pixm) return;
    XSetWindowBackgroundPixmap (dpy (), win (), _pixm);

    switch (mtype)
    {
    case VUM:
    case CVUM:
        _col1 = _col_vum;
	_nw = 1.0;
	break;
    case BBC:
    case EBU:
    case DIN:
        if (nchan == 2)
	{
            _col1 = _col_ppmL;
            _col2 = _col_ppmR;
	}
	else _col1 = _col_ppm;
	_nw = 1.5;
	break;
    case CPPM:
    case CDIN:
        _col1 = _col_ppm;
	_nw = 1.5;
	break;
    default:
	return;
    }

    switch (frame)
    {
    case RECT:
	_xs = 300;
	_ys = 170;
	_xc = 149.5;
	_yc = 209.5;
	_r1 = 0;
	_r2 = 180;
	_yb = 134;
	_xl = 15;
	_yl = _ys - 13;
	_nw += 0.4;
	break;
    case SMALL:
	_xs = 219;
	_ys = 125;
	_xc = 109.5;
	_yc = 149.5;
	_r1 = 0;
	_r2 = 128;
	_yb = 98;
	_xl = 10;
	_yl = _ys - 9;
	_nw += 0.2;
	break;
    case MBRL:
	_xs = 220;
	_ys = 220;
	_xc = 109.5;
	_yc = 168.5;
	_r1 = 19;
	_r2 = 108;
	_yb = _yc;
	_xl = _xc;
	_yl = _ys - 30;
	_nw += 0.2;
	break;
    case MBRS:
	_xs = 180;
	_ys = 180;
	_xc =  89.5;
	_yc = 138.5;
	_r1 = 16;
	_r2 = 88;
        _yb = _yc;
	_xl = _xc;
	_yl = _ys - 22;
	break;
    default:
	return;
    }	

    if (_cairosurf == 0)
    {
        _cairosurf = cairo_xlib_surface_create (dpy (), 0, dvi (), 50, 50);
        _cairotype = cairo_create (_cairosurf);
    }
    x_resize (_xs, _ys);
    x_add_events (ExposureMask);
    x_map ();
}

 
Meterwin::~Meterwin (void)
{
}

 
void Meterwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case Expose:
	expose ((XExposeEvent *) E);
	break;
    }  
}


void Meterwin::expose (XExposeEvent *E)
{
    if (E->count) return;
    redraw ();
}


void Meterwin::set_val (float v1, float v2)
{
    switch (_mtype)
    {
    case VUM:
	_val1 = vum_map (v1);
	break;

    case DIN:
	if (_col1) _val1 = din_map (v1);
	if (_col2) _val2 = din_map (v2);
	break;

    case BBC:
    case EBU:
	if (_col1) _val1 = ppm_map (v1);
	if (_col2) _val2 = ppm_map (v2);
	break;

    case CVUM:
    case CPPM:
    case CDIN:
	_val1 = corr_map (v1);
	break;

    default:
	_val1 = 0;
	_val2 = 0;
	;	
    }
    update (true);	
}


void Meterwin::update (bool clear)
{
    if (clear) XClearArea (dpy (), win (), 0, 0, _xs, _yb + 1, False);
    if (_col1) needle (_val1, _col1);
    if (_col2) needle (_val2, _col2);
}


void Meterwin::redraw (void)
{
    X_draw D (dpy (), win (), dgc (), xft ());

    XClearWindow (dpy (), win ());
    update (false);
    if (_label)
    {
	D.setfont (XftFonts [F_LABEL]);
	D.setcolor (XftColors [C_MAIN_FG]);
	D.move (_xl, _yl);
	D.drawstring (_label, (_frame <= SMALL) ? -1 : 0);
    }
}


void Meterwin::needle (float val, double *col)
{
    X_draw D (dpy (), win (), dgc (), 0);
    float  c, s;

    if (val < 0.00f) val = 0.00f;
    if (val > 1.05f) val = 1.05f;
    val = (val - 0.5f) * 1.5708f;
    c = cosf (val);
    s = sinf (val);
    cairo_xlib_surface_set_drawable (_cairosurf, win(), _xs, _ys);
    cairo_new_path (_cairotype);
    if (_frame <= SMALL) cairo_move_to (_cairotype, _xc + (_yc - _yb) * s / c, _yb);
    else cairo_line_to (_cairotype, _xc + s * _r1, _yc - c * _r1);
    cairo_line_to (_cairotype, _xc + s * _r2, _yc - c * _r2);
    cairo_set_source_rgb (_cairotype, col [0], col [1], col [2]);
    cairo_set_line_width (_cairotype, _nw);
    cairo_stroke (_cairotype);
}


Pixmap Meterwin::findpixmap (int32_t mtype, int frame, const char *pngpath)
{
    Jmpixmap  *jmpix;
    Pixmap     pixmap;

    for (jmpix = _pixmaps; jmpix; jmpix = jmpix->_next)
    {
	if ((jmpix->_mtype == mtype) && (jmpix->_frame == frame)) return jmpix->_pixmap;
    }
    pixmap = makepixmap (mtype, frame, pngpath);
    if (pixmap)
    {
        jmpix = new Jmpixmap;
        jmpix->_next = _pixmaps;
        jmpix->_mtype = mtype;
        jmpix->_frame = frame;
	jmpix->_pixmap = pixmap;
        _pixmaps = jmpix;
    }
    return pixmap;
}


Pixmap Meterwin::makepixmap (int mtype, int frame, const char *pngpath)
{
    int         dx, dy;
    XImage     *image;
    Pixmap      pixmap;
    char        s [1024];
    const char  *pt, *pf;	 

    pt = pf = 0;

    switch (mtype)
    {
    case VUM:
        pt = "vu-meter";
        break;
    case BBC:
        pt = "bbc-meter";
        break;
    case EBU:
        pt = "ebu-meter";
        break;
    case DIN:
        pt = "din-meter";
        break;
    case CVUM:
        pt = "vu-scorr";
        break;
    case CPPM:
        pt = "ppm-scorr";
        break;
    case CDIN:
        pt = "din-scorr";
        break;
    }

    switch (frame)
    {
    case RECT:
	pf = "3";
	break;
    case SMALL:
	pf = "4";
	break;
    case MBRL:
	pf = "1";
	break;
    case MBRS:
	pf = "2";
	break;
    }

    if (! pt || ! pf) return 0;
    sprintf (s, "%s/%s%s.png", pngpath, pt, pf);
    image = png2img (s, disp (), _bgnd);
    if (! image) return 0;
    dx = image->width;
    dy = image->height;
    pixmap = XCreatePixmap (dpy (), win (), dx, dy, disp ()->depth ());
    XPutImage (dpy (), pixmap, dgc (), image, 0, 0, 0, 0, dx, dy);
    delete[] image->data;
    image->data = 0;
    XDestroyImage (image);
    return pixmap;
}


void Meterwin::cleanup (Display *dpy)
{
    Jmpixmap *jmpix;

    while (_pixmaps)
    {
        jmpix = _pixmaps;
	_pixmaps = jmpix->_next;
	XFreePixmap (dpy, jmpix->_pixmap);
	delete jmpix;
    }
}
