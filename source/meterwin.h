// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#ifndef __METERWIN_H
#define	__METERWIN_H


#include <stdint.h>
#include <math.h>
#include <clxclient.h>
#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>


class Jmpixmap
{
public:

    Jmpixmap   *_next;
    int         _mtype;
    int         _frame;
    Pixmap      _pixmap;
};


class Meterwin : public X_window
{
public:

    enum { VUM, BBC, EBU, DIN, CVUM, CPPM, CDIN };
    enum { RECT, SMALL, MBRS, MBRL };

    Meterwin (X_window *parent, int xpos, int ypos, XftColor *bgnd,
              int32_t mtype, int frame, int nchan, const char *pngpath,
              const char *label = 0);
    ~Meterwin (void);
    Meterwin (const Meterwin&);
    Meterwin& operator=(const Meterwin&);

    int xs (void) { return _xs; }
    int ys (void) { return _ys; }
    void set_val (float v1, float v2);
    Pixmap findpixmap (int mtype, int frame, const char *pngpath);
    Pixmap makepixmap (int mtype, int frame, const char *pngpath);

    static void cleanup (Display *dpy);

private:

    void handle_event (XEvent *);
    void expose (XExposeEvent *E);
    void update (bool clear);
    void redraw (void);
    void needle (float val, double *col);

    float vum_map (float v)
    {
	return 7.07946f * v;
    }

    float ppm_map (float v)
    {
	v *= 3.17f; 
        if (v < 0.1f) return v * 0.855f;
	else return 0.3f * logf (v) + 0.77633f;
    }

    float din_map (float v)
    {
	v = sqrtf (sqrtf (2.8284f * v)) - 0.1885f; 
	return (v < 0.0f) ? 0.0f : v;
    }

    float corr_map (float v)
    {
	return 0.5f * (1.0f + v);
    }

    int          _mtype;
    int          _frame;
    const char  *_label;
    XftColor    *_bgnd;
    int          _xs;
    int          _ys;
    double       _xc;
    double       _yc;
    int          _r1;
    int          _r2;
    int          _yb;
    int          _xl;
    int          _yl;
    Pixmap       _pixm;
    double       _nw;
    double      *_col1;
    double      *_col2;
    float        _val1;
    float        _val2;

    static      cairo_t          *_cairotype;
    static      cairo_surface_t  *_cairosurf;
    static      Jmpixmap         *_pixmaps;
    static      double            _col_vum [3];
    static      double            _col_ppm [3];
    static      double            _col_ppmL [3];
    static      double            _col_ppmR [3];
};


#endif
