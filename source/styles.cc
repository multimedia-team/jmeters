// ----------------------------------------------------------------------
//
//  Copyright (C) 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------


#include "styles.h"


XftColor        *XftColors [NXFTCOLORS];
XftFont         *XftFonts [NXFTFONTS];


XftColor *blackorwhite (XftColor *C)
{
    float v;

    v = 0.23f * C->color.red + 0.70f * C->color.green + 0.07f * C->color.blue;
    return (v > 25e3f) ? XftColors [C_BLACK] : XftColors [C_WHITE];
}


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_BLACK]    = disp->alloc_xftcolor ("black", 0);
    XftColors [C_WHITE]    = disp->alloc_xftcolor ("white", 0);
    XftColors [C_MAIN_BG]  = disp->alloc_xftcolor (xrm->get (".color.main.bg",   "gray20" ),  0);
    XftColors [C_MAIN_FG]  = disp->alloc_xftcolor (xrm->get (".color.main.fg",   "white"  ),  0);
    XftFonts [F_LABEL]     = disp->alloc_xftfont (xrm->get (".font.label", "suse:bold:pixelsize=14"));
}


void styles_fini (X_display *disp)
{
}
